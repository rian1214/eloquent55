<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SampleModel;

class SampleController extends Controller
{
    public function __construct(){
      $this->middleware('auth');
    }

    public function index(){

    }

    public function create(){

    }

    public function store(Request $r){

    }

    public function show(){

    }
}
