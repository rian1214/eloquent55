<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'Items';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'stock', 'price'];

}
